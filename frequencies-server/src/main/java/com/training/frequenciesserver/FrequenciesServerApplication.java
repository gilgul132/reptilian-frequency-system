package com.training.frequenciesserver;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class FrequenciesServerApplication {
	private static final int RESPONSE_SIZE_IN_BYTES = 32;

	public static void main(String[] args) {
		SpringApplication.run(FrequenciesServerApplication.class, args);
	}

	@GetMapping("/frequency")
	public ResponseEntity<byte[]> generateFrequency() {
		byte[] frequency = new byte[RESPONSE_SIZE_IN_BYTES];

		new Random().nextBytes(frequency);

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(frequency);
	}
}
