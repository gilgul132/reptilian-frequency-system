import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FrequencyGeneratorComponent } from './frequency-generator/frequency-generator.component';

@NgModule({
  declarations: [
    AppComponent,
    FrequencyGeneratorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
