import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-frequency-generator',
  templateUrl: './frequency-generator.component.html',
  styleUrls: ['./frequency-generator.component.css']
})
export class FrequencyGeneratorComponent implements OnInit {
  readonly serverUrl = environment.serverUrl;

  constructor() { }

  ngOnInit(): void {
  }

}
